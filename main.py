import random
### Function task 1
M = 4

# Task 1A:  printing board (5 marks)
def printBoard(board, rowSum, colSum):
    print("   |", end="")
    positivesRow = []
    positivesColumn = []
    for i in range(M):
        positivesRow.append(i+1)
        positivesColumn.append(i+1)

    negativesRow = rowSum
    negativesColumn = colSum
    workingBoard = board

    for p in range(len(positivesColumn)):
        val = positivesColumn[p];
        if val == -1:
            val = 0

        if val != 0:
            print(" " + str(val) + " |", end="")
        else:
            print("   |", end="")

    print("")

    for i in range(M):
        if i == 0:
            print("---", end="")
            for x in range(len(positivesColumn) + 1):
                print("|---", end="")
            print()
        else:
            print("---", end="")
            for j in range(M):
                print("|", end="")
                print("---", end="")
            print("|---")

        for j in range(M):
            value = workingBoard[i][j]
            pipe = ""
            if j == 0:
                if positivesRow[i] == -1:
                    pipe = "   |"
                else:
                    pipe = " " + str(positivesRow[i]) + " |"
            else:
                pipe = "|"
            print(pipe + " " + str(value) + " ", end="")

        if negativesRow[i] == -1:
            print("|  ")
        else:
            print("| " + str(negativesRow[i]));

        if i == (M - 1):
            print("---", end="");
            for x in range(len(positivesColumn) + 1):
                print("|---", end="")
            print();

    print("   |", end="");
    for p in range(len(negativesColumn)):
        val = negativesColumn[p];
        if negativesColumn[p] == -1:
            val = 0;
        print(" " + str(val) + " |", end="")

# Task 1B:  converting a bit list into a board (5 marks)
def list2board(arr):
    newArr = [arr[r * M:(r + 1) * M] for r in range(0, M)]
    return newArr

# Task 1C: Check solution (5 marks)
def isSolution(board, rowSum, colSum):
    solution = True

    for i in range(M):
        if calculateRow(i) != rowSum[i] or calculateCol(i) != colSum[i]:
            solution = False

    # for i in range(M):
    #
    #     # Check horizontal
    #     total = 0
    #     for j in range(M):
    #         if board[i][j] == 1:
    #             total += (j+1)
    #
    #     if total != rowSum[i]:
    #         solution = False
    #         break
    #
    # for i in range(M):
    #
    #     # Check vertical
    #     total = 0
    #     for j in range(M):
    #         if board[j][i] == 1:
    #             total += (j+1)
    #
    #     if total != colSum[i]:
    #         solution = False
    #         break

    return solution

# Helper function to Calculate Row and/or column
def calculateCol(col):
    total = 0
    for i in range(M):
        if board[i][col] == 1:
            total += (i+1)
    return total

def calculateRow(row):
    total = 0
    for i in range(M):
        if board[row][i] == 1:
            total += (i+1)
    return total

def fixCol(col):
    for i in range(M):
        if (i+1) > colSum[col]:
            board[i][col] = 0
    return

def fixRow(row):
    for i in range(M):
        if (i+1) > rowSum[row]:
            board[row][i] = 0
    return

def solveCol(j):
    while calculateCol(j) != colSum[j]:
        for i in range(M):
            if board[i][j] != 0:
                board[i][j] = 1
                # print(str(calculateCol(j))+" > "+ str(colSum[j]))
                if calculateCol(j) > colSum[j]:
                    # if j == 0:
                    #     board[M - 1][j] = -1
                    # else:
                    board[i-1][j] = -1


def solveRow(i):
    while calculateRow(i) != rowSum[i]:
        # printBoard(board, rowSum, colSum)
        # print("\n\n")

        for j in range(M):
            if board[i][j] != 0:
                board[i][j] = 1
                if calculateRow(i) > rowSum[i]:
                    # if i == 0:
                    #     board[i][M-1] = -1
                    # else:
                    board[i][j - 1] = -1

def bruteforce2(rowSum, colSum):
    for i in range(M):
        fixRow(i)
    for i in range(M):
        fixCol(i)

    # printBoard(board, rowSum, colSum)
    while not isSolution(board, rowSum, colSum):
         for i in range(M):
             solveCol(i)
         for i in range(M):
             solveRow(i)

    for i in range(M):
        for j in range(M):
            if board[i][j] == -1:
                board[i][j] = 0

### End Function task 1


### Function task 2

# Task 2A: Brute‐force (25 marks)
def bruteforce(rowSum, colSum):
    combination = [[[0 for col in range(100)] for col in range(100)] for row in range(25)]
    bit = [0] * 10
    sum = 0
    for i in range(1 << M):
        bit[0] += 1
        sum = 0

        # Create combination 0 and 1 with dimension size for example (5)
        for j in range(M):
            if bit[j] == 2:
                bit[j] = 0
                bit[j + 1] += 1
            if bit[j] == 1:
                sum += (j + 1)

        # Save combination to 3d array
        combination[sum][0][0] += 1
        # print("combination["+str(sum)+"][0][0] = "+str(combination[sum][0][0]))
        ccount = combination[sum][0][0]
        for j in range(M):
            combination[sum][ccount][j] = bit[j]
            # print("combination[" + str(sum) + "]["+str(ccount)+"]["+str(j)+"] = " + str(combination[sum][0][0]))

    # for i in range(len(combination)):
    #     # print("I = " + str(i))
    #     for j in range(len(combination[i])):
    #         print("I  = "+str(i)+" | J = "+str(j))
    #         for k in range(len(combination[i])):
    #             print("I  = "+str(i)+" | J = "+str(j)+" | K = "+str(k))

    attemp = 0
    key = [0] * 10
    isValid = False

    for i in range(M):
        key[i] = 1

    while not isValid:
        key[0] += 1
        # print(key)

        # printBoard(board, rowSum, colSum)
        # print('\n')

        for i in range(M):
            rsum = rowSum[i]
            if key[i] > combination[rsum][0][0]:
                key[i] = 1
                key[i + 1] += 1

            for j in range(M):
                #print("combination["+str(rsum)+"]["+str(key[i])+"]["+str(j)+"] : "+str(combination[rsum][key[i]][j]))
                board[i][j] = combination[rsum][key[i]][j]
                if board[i][j] == 1:
                    attemp += 1

        count = 0
        for j in range(M):
            csum = 0
            for k in range(M):
                if board[k][j] == 1:
                    csum += (k + 1)
            if csum == colSum[j]:
                count += 1

        # print("COUNT == "+str(count))
        if count == M:
            isValid = True
            break
    return


### Test Function Task 1
def testTask1A():
    printBoard(board, rowSum, colSum)

def testTask1B():
    # Test Task 1B
    printBoard(board, rowSum, colSum)

def testTask1C():
    print(isSolution(board, rowSum, colSum))

def testTask2():
    ### Test Function Task 2
    for i in range(M):
        fixRow(i)
    for i in range(M):
        fixCol(i)

    bruteforce(rowSum, colSum)
    printBoard(board, rowSum, colSum)


### Start Test task 1A
# M = 4
# rowSum = [8,9,6,9]
# colSum = [4,9,10,7]
# board = [[1, 0, 0, 0], [0, 0, 1, 1], [0, 0, 1, 1], [1, 0, 1, 1]]
# testTask1A()
### End Task 1A

### Start test task 1B
# L = [1,0,0,0,0,0,1,1,0,0,1,1,1,0,1,1]
# board = list2board(L)
# rowSum = [1, 7, 7, 8]
# colSum = [5, 0, 9, 9]
# testTask1B()
### End test task 1B


### Start test task 1C
# Test Task 1C
# False solution
# board = [[1, 1, 0, 0], [0, 0, 1, 1], [0, 0, 1, 1], [1, 0, 1, 1]]
# True solution
# board = [[1, 0, 0, 0],[0, 0, 1, 1],[0, 0, 1, 1],[1, 0, 1, 1]]
# M = 4
# rowSum = [1, 7, 7, 8]
# colSum = [5, 0, 9, 9]
# testTask1C()
### End test task 1C

### Start Test task 2 with dimension 4
# M = 4
# rowSum = [1, 7, 7, 8]
# colSum = [5, 0, 9, 9]
# board = [
#     [-1, -1, -1, -1],
#     [-1, -1, -1, -1],
#     [-1, -1, -1, -1],
#     [-1, -1, -1, -1],
# ]
# testTask2()
### End Test task 2 with dimension 4

### Start Test task 2 with dimension 4
# M = 4
# rowSum = [8,9,6,9]
# colSum = [4,9,10,7]
# board = [
#     [-1, -1, -1, -1],
#     [-1, -1, -1, -1],
#     [-1, -1, -1, -1],
#     [-1, -1, -1, -1],
# ]
# testTask2()
### End Test task 2 with dimension 4

### Start Test task 2 with dimension 4
# M = 4
# rowSum = [4,5,3,5]
# colSum = [10,3,1,6]
# board = [
#     [-1, -1, -1, -1],
#     [-1, -1, -1, -1],
#     [-1, -1, -1, -1],
#     [-1, -1, -1, -1],
# ]
# testTask2()
### End Test task 2 with dimension 4

### Start Test task 2 with dimension 5
M = 5
rowSum = [6,0,6,14,12]
colSum = [1,7,9,12,10]
board = [
    [-1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1]
]
testTask2()
### End Test task 2 with dimension 5


